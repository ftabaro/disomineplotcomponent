# DisominePlotComponent

## Dependencies
- node 6.11.2
- npm 3.10.10
- D3

### Dev-dependencies
- Babel
- Webpack (with dev-server)

## Install and run dev-server
```bash
$  npm install
$  npm start
```

open browser and go to `http://localhost:8080`.

## Deploy

```
$ npm run build
```

To edit build configuration, edit `webpack.config.js`.