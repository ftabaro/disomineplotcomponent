import Plot from './plot'

export default class PlotComponent {

  constructor(target, data, options = null) {

    // pseudo-global variables
    const features = ['disomine', 'efoldmine', 'backbone', 'sidechain'];

    // define listeners as anonymous functions. Reduces scoping problems
    let updatePlot = () => {

      let selectValue = document.getElementById('pSele').value;
      let selected = this.DATA.filter(protein => protein.proteinID === selectValue)[0];
      let available_data = Object.keys(selected);

      let nodes = document.getElementsByName('featureSele');
      nodes.forEach(e => {
        let label = document.getElementById(`label-${e.value}`);
        if (available_data.includes(e.value)) {
          e.style.display = 'inline-block';
          label.style.display = 'inline-block';
        } else {
          e.style.display = 'none';
          label.style.display = 'none';
        }

      });
      let radio = [].filter.call(nodes, e => e.checked)[0];

      let resultArea = document.getElementById('resultArea');
      resultArea.innerHTML = '';

      resultArea.innerHTML = `<div>Selected protein: ${selected.proteinID}</div><div>Sequence length: ${selected.sequence.length}</div><div>Selected feature: ${radio.value}</div>`

      this.plot.updatePlot(radio.value, selected);
    };

    // select target
    let container = document.getElementById(target);

    // assign attribute
    this.DATA = JSON.parse(JSON.stringify(data));

    // new plot object
    if (options && options.hasOwnProperty('W') && options.hasOwnProperty('H')) {
      this.plot = new Plot(container, options.W, options.H * 0.75);
    } else {
      this.plot = new Plot(container);
    }

    // init seqLen, create top radio container
    let seqLen,
       form = document.createElement('form');
    form.id = 'topform';
    form.style = 'display: flex; justify-content: space-between; width: 100%; padding-top: 0.5em';
    container.appendChild(form);

    // create select + options and measure seqLen
    let pSelect = document.createElement('select');
    pSelect.id = 'pSele';
    pSelect.title = 'Select protein';
    pSelect.addEventListener('change', updatePlot);

    data.forEach(protein => {
      let option = document.createElement('option');
      option.value = option.textContent = protein.proteinID;
      pSelect.appendChild(option);

      seqLen = protein.sequence.length;
    });
    form.appendChild(pSelect);

    // create checkboxes
    let cbContainer = document.createElement('div');
    cbContainer.id = 'cbContainer';
    cbContainer.title = 'Select feature';
    form.appendChild(cbContainer);
    features.forEach((k, i) => {
      let cb = document.createElement('input');
      cb.type = 'radio';
      cb.name = 'featureSele';
      cb.value = k;
      cb.id = `f${i}`;
      cb.checked = k === 'disomine';
      cb.addEventListener('click', updatePlot);

      let label = document.createElement('label');
      label.htmlFor = `sele${i}`;
      label.id = `label-${k}`;
      label.appendChild(document.createTextNode(`${k.charAt(0).toUpperCase()}${k.slice(1)}`));

      cbContainer.appendChild(cb);
      cbContainer.appendChild(label);
    });

    // prepare empty div to display text
    let resultArea = document.createElement('div');
    resultArea.id = 'resultArea';
    resultArea.style = 'padding-top: .5em';
    container.appendChild(resultArea);

    let helpBox = document.createElement('div');
    helpBox.style = 'padding-top: .5em';
    helpBox.innerHTML = 'Click and hold to zoom; double click to reset. Mouse hover line to display details.';
    container.appendChild(helpBox);

    updatePlot()
  }
}