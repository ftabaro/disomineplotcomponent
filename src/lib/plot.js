import * as d3 from 'd3';

export default class Plot {

  constructor(targetNode, W = 500, H = 300) {

    // define brushend functions as anonymous functions. //

    const _brushended = () => {
      if (d3.event.sourceEvent !== null && typeof d3.event.sourceEvent.target !== "function") {
        let extent = d3.event.selection;
        if (extent) { // zooming

          //modify scale
          extent = extent.map(Math.round);
          let axcoord = extent.map(this.x.invert, this.x);
          if (axcoord[1] - axcoord[0] > 5) this.x.domain(axcoord)

        } else {

          // reset scale
          this.x.domain(this.x0);
          console.warn("Zoom reset")
        }

        // perform transition
        _zoom();

        // remove brush now that transition is complete
        this.svg.select(".brush").call(this.brush.move, null);

      }
    };

    const _zoom = () => {

      // define transition and apply to all elements
      let t = this.svg.transition().duration(750);

      this.svg.select(".axis--x").transition(t).call(this.xAxis);

      this.svg.selectAll(".dotplot").transition(t)
         .attr("cx", (_, i) => this.x(i + 1))
         .attr("cy", d => this.y(d));

      this.svg.selectAll('.line')
         .transition(t)
         .attr("d", d3.line()
            .x((_, i) => this.x(i + 1) < 0 ? null : this.x(i + 1))
            .y(d => this.y(d))
         );

      this.svg.selectAll('.grid--x').transition(t)
         .attr("transform", "translate(0," + this.height + ")")
         .call(this._makeXGridlines(this.sequence)
            .tickSize(-this.height)
            .tickFormat(""));
    };

    // define margins
    this.margin = {top: 40, right: 20, bottom: 45, left: 60};
    this.width = W - this.margin.left - this.margin.right;
    this.height = H - this.margin.top - this.margin.bottom;

    // append svg
    this.svg = d3.select(targetNode).append('svg')
       .attr('width', W).attr('height', H)
       .append("g")
       .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    // create x axis
    this.x = d3.scaleLinear().range([0, this.width]);
    this.xref = d3.scaleLinear().range([0, this.width]);

    // create y axis
    this.y = d3.scaleLinear().range([this.height, 0]);

    // define line
    this.valueline = d3.line()
       .x((_, i) => this.x(i + 1))
       .y(d => this.y(d));

    // define brush
    this.brush = d3.brushX()
       .extent([[this.xref(0), this.y(1)], [this.width, this.y(0)]])
       .on("end", _brushended);

    this.dirty = false;
  }

  updatePlot(featureName, feature) {

    let plotData = feature[featureName];
    let sequence = this.sequence = feature['sequence'];
    let unp = feature['proteinID'];

    if (this.dirty) this._clear();
    this.dirty = true;

    // Scale the range of the data
    this.x0 = [0, plotData.length];
    this.y0 = [0, 1];
    this.x.domain(this.x0);
    this.y.domain(this.y0);

    this.xAxis = d3.axisBottom(this.x);
    this.yAxis = d3.axisLeft(this.y);

    // display title
    this.svg.append('g')
       .attr("class", "title")
       .append("text")
       .attr("x", (this.width / 2))
       .attr("y", 0 - (this.margin.top / 2))
       .attr("text-anchor", "middle")
       .style("font-size", "22px")
       .style("font-weight", "bold")
       .text(`${featureName.charAt(0).toUpperCase()}${featureName.slice(1)} profile for ${unp}`);

    // add the X gridlines
    this.svg.append("g")
       .attr("class", "grid grid--x")
       .attr("transform", "translate(0," + this.height + ")")
       .call(this._makeXGridlines(sequence)
          .tickSize(-this.height)
          .tickFormat("")
       );

    // add the Y gridlines
    this.svg.append("g")
       .attr("class", "grid grid--y")
       .call(this._makeYGridlines()
          .tickSize(-this.width)
          .tickFormat("")
       );

    // Add the valueline path.
    this.svg.append("path")
       .data([plotData])
       .attr("class", `line ${featureName}--line`)
       .attr("d", this.valueline);

    // Add threshold line
    this.svg.append("line")
        .attr("x1", this.xref(0))
        .attr("y1", (1 - 0.38) * (this.height))
        .attr("x2", this.width)
        .attr("y2", (1 - 0.38) * (this.height))
        .attr("class", "thr--line");

    // append x axis
    this.svg.append("g")
       .attr("class", "axis axis--x")
       .attr("transform", "translate(0," + this.height + ")")
       .call(this.xAxis)
       .append("text")
       .attr("x", this.width / 2)
       .attr("y", this.margin.bottom * 0.9)
       .attr("dx", "0.32em")
       .attr("fill", "#000")
       .attr("font-weight", "bold")
       .attr("text-anchor", "start")
       .text("Residue Index");

    // append y axis
    this.svg.append("g")
       .attr("class", "axis axis--y")
       .call(this.yAxis)
       .append("text")
       .attr("x", -this.margin.left * 0.8)
       .attr("y", this.height / 2)
       .attr("dx", "0.32em")
       .attr("fill", "#000")
       .attr("font-weight", "bold")
       .attr("text-anchor", "start")
       .attr("transform", `rotate(-90, ${-this.margin.left * 0.8}, ${this.height / 2})`)
       .text("Score");

    // append brush
    this.svg.append("g")
       .attr("class", "brush")
       .call(this.brush);
    // .call(this.brush.move, this.x.range());

    // Add the scatterplot
    this.svg.selectAll("dot")
       .data(plotData)
       .enter()
       .append("circle")
       .attr("class", `dotplot ${featureName}--dot`)
       .attr("r", 10)
       .attr("cx", (_, i) => this.x(i + 1))
       .attr("cy", d => this.y(d))
       .attr("opacity", 0)
       .on('mouseover', function () {
         let transition = d3.transition().duration(200);
         d3.select(this).transition(transition).attr('opacity', 0.5)
       })
       .on('mouseleave', function () {
         let transition = d3.transition().duration(200);
         d3.select(this).transition(transition).attr('opacity', 0)
       })
       .append("title")
       .text((d, i) => `${sequence.charAt(i).toUpperCase()}${i + 1}: ${d.toFixed(2)}`);
  }

  // gridlines in x axis function
  _makeXGridlines(sequence) {
    return d3.axisBottom(this.x)
       .ticks(Math.floor(sequence.length / 20))
  }

  // gridlines in y axis function
  _makeYGridlines() {
    return d3.axisLeft(this.y)
       .ticks(5)
  }

  _clear() {
    // for some reason selecting and removing of axes caused some residues. Let's do it a few extra times ;)
    let c = this.sequence.length * 2;
    while (c > 0) {
      d3.select("g.axis").remove();
      d3.select("g.title").remove();
      d3.select("path.line").remove();
      d3.select("line.thr--line").remove();
      d3.select("g.grid").remove();
      d3.select("g.brush").remove();
      d3.select(".dotplot").remove();

      c--
    }
  }
}