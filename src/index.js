import DATA from './example2.json';
import PlotComponent from './lib/plotComponent';

(function component() {

  let elm = document.getElementById('target');
  let W = elm.clientWidth;
  let H = elm.clientHeight;
  console.log(W,H);

  const data = DATA.results;
  let cmpnt = new PlotComponent('target', data, {W: W, H: H});

})();
