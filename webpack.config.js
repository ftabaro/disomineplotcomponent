const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  entry: {
    app: './src/index.js',
    libPlot: "./src/lib/plotComponent.js"
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: ["lib", "[name]"],
    libraryTarget: "var",
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  resolve: {
         modules: [path.resolve('./node_modules'), path.resolve('./src')],
         extensions: ['.json', '.js', '.ts', '.tsx']
     },

  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/},
      {test: /\.exec\.js$/, loader: ['script-loader']},
      {test: /\.tsx?$/, exclude: /(node_modules|bower_components)/, loader: 'ts-loader'}
    ]
  },
  plugins: [
    new CleanWebpackPlugin('./dist'),
    HtmlWebpackPluginConfig
  ]
}
;
